import ratpack.groovy.template.MarkupTemplateModule

import static ratpack.groovy.Groovy.groovyMarkupTemplate
import static ratpack.groovy.Groovy.ratpack

ratpack {
  bindings {
    module MarkupTemplateModule
    add(ResponceHandler.class,new ResponceHandler())
  }
  serverConfig {
    port(8080)
  }

  handlers {
    get {
      render "val"
    }

    files { dir "public" }
  }
}
