package handlers.user.method.post;

import Items.users.User;
import database.mycouchbase.UserRecords;
import ratpack.exec.Blocking;
import ratpack.handling.Context;
import ratpack.handling.Handler;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static ratpack.jackson.Jackson.jsonNode;

/**
 * Created by Oluwole on 15/01/2017.
 */
public class RegisterUserHandler implements Handler {
    @Override
    public void handle(Context ctx) throws Exception {

        ctx.parse(jsonNode()).map(jsonNode -> jsonNode).then(
                requestBody ->{
                    Blocking.exec(() -> {
                        //UUID id = UUID.randomUUID();

                        User user = new User(requestBody);
                        if (user.getPassword()==null){
                            ctx.getResponse().status(404).contentType("application/json").send("{\"MSG\": \"Password field is missing\"}");
                            return;
                        }

                        if (user.getId()==null){
                            user.setId(generateUniqueId());
                        }

                        //DatabaseStrategyInterface database = CouchbaseSingleton.getInstance();

                        UserRecords.save(user);
                        ctx.getResponse().status(200).contentType("application/json").send(user.jsonify().toString());
                    });
                }
        );
    }
    private static String generateUniqueId(){
        UUID uuid = UUID.randomUUID();
        Optional<List<User>> userOptional = UserRecords.searchById(uuid.toString());
        if (userOptional.isPresent())
            return generateUniqueId();
        return uuid.toString();
    }
}
