package handlers.user.method.post;

import Items.users.User;
import database.mycouchbase.UserRecords;
import ratpack.exec.Blocking;
import ratpack.handling.Context;
import ratpack.handling.Handler;

import java.util.List;
import java.util.Optional;

import static ratpack.jackson.Jackson.jsonNode;

/**
 * Created by Oluwole on 15/01/2017.
 */
public class LoginUserHandler implements Handler {
    @Override
    public void handle(Context ctx) throws Exception {

//        ctx.parse(Form.class).map(f -> {
//
//        });
        ctx.parse(jsonNode()).map(jsonNode -> jsonNode).then(
                requestBody ->{
                    Blocking.exec(() -> {
                        User user = new User(requestBody);
                        Optional<User> res = authUser(user.getEmail(),user.getPassword());
                        ctx.getResponse().getHeaders().add("Access-Control-Allow-Origin","http://localhost:63342");
                        if(res.isPresent()) {

                            ctx.getResponse().status(200).contentType("application/json").send(res.get().jsonify().toString());
                        }else
                            ctx.getResponse().status(400).contentType("application/json").send("{\"MSG\": \"unauthorized login\"}");
                    });
                }
        );
    }


    private static Optional<User> authUser(String email,String password){
        Optional<List<User>> users = UserRecords.searchByEmail(email);
        if (users.isPresent()){
            for(User user : users.get()){
                if (user.getPassword().equals(password))
                    return Optional.of(user);
            }
        }
        return Optional.empty();
    }
}
