package handlers.meme.method.post;

import Items.memes.Meme;
import database.mycouchbase.MemeRecords;
import ratpack.exec.Blocking;
import ratpack.handling.Context;
import ratpack.handling.Handler;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static ratpack.jackson.Jackson.jsonNode;

/**
 * Created by Oluwole on 08/04/2017.
 */
public class SaveMemeHandler implements Handler {
    @Override
    public void handle(Context ctx) throws Exception {
        ctx.parse(jsonNode()).map(jsonNode -> jsonNode).then(
                requestBody ->{
                    Blocking.exec(() -> {
                        Meme meme = new Meme(requestBody);
                        if (meme.getUrl()!=null) {
                            if (meme.getId() == null) {
                                meme.setId(generateUniqueId());
                            }
                            //ctx.getResponse().getHeaders().add("Access-Control-Allow-Origin","http://localhost:63342");
                            MemeRecords.save(meme);
                            ctx.getResponse().contentType("application/json");
                            ctx.getResponse().status(200).send("{\"MSG\": \"resource was saved\"}");
                            ;
                        }else {
                            ctx.getResponse().contentType("application/json");
                            ctx.getResponse().status(404).send("{\"MSG\": \"Missing url field\"}");
                        }
                    });
                }
        );
    }

    private static String generateUniqueId(){
        UUID uuid = UUID.randomUUID();
        Optional<List<Meme>> memeOptional = MemeRecords.searchById(uuid.toString());
        if (memeOptional.isPresent())
            return generateUniqueId();
        return uuid.toString();
    }
}