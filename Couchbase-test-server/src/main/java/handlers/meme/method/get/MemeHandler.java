package handlers.meme.method.get;

import Items.memes.Meme;
import Items.memes.UserMemeRelation;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import database.mycouchbase.MemeRecords;
import database.mycouchbase.UserMemeRelationRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.exec.Blocking;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import utils.JsonClassHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by Oluwole on 18/12/2016.
 */
public class MemeHandler implements Handler {
    private static final Logger LOGGER = LoggerFactory.getLogger(MemeHandler.class);
    private final int MAX_RESULTS = 10;



    private Optional<String> getMemeByName(Context ctx){
            Optional<List<Meme>> memes = MemeRecords.searchByName(ctx.getPathTokens().get("memeParam"));
            List<Meme> memeList = new ArrayList<Meme>();
            String response = "";
            if (memes.isPresent()){
                memeList = new ArrayList<Meme>(memes.get());
                List<String> memesJson = new ArrayList<String>();
                for (Meme meme : memeList){
                    memesJson.add(meme.jsonify().toString());
                }
                response+=JsonClassHelper.getJsonList(null,memesJson);
                //response+="}";
            }
            else
                return Optional.empty();

            return Optional.of(response);
    }


    private Optional<String> getMemeById(Context ctx){
        Optional<List<Meme>> memes = MemeRecords.searchById(ctx.getPathTokens().get("memeParam"));
        List<Meme> memeList = new ArrayList<Meme>();
        String response = "";
        if (memes.isPresent()){
            memeList = new ArrayList<Meme>(memes.get());

            for (Meme meme : memeList){
                response+=(meme.jsonify().toString());
            }

        }
        else
            return Optional.empty();

        return Optional.of(response);
    }

    private Optional<String> getFullMemeByName(Context ctx){
        //meme+all attributes in other tables
        String result = getMemeByName(ctx).get();

        if (ctx.getPathTokens().containsKey("userId")){
            String usrId = ctx.getPathTokens().get("userId");
            String responce ="";
            List<String> memesJson = new ArrayList<String>();
            for (Object obj:JsonArray.fromJson(result)){
                Meme meme = new Meme((JsonObject)obj);
                Optional<UserMemeRelation> relations = UserMemeRelationRecords.searchById(meme.getId(),usrId);
                if (relations.isPresent())
                    meme.setLiked(true);
                memesJson.add(meme.jsonify().toString());
            }

            responce+=JsonClassHelper.getJsonList(null,memesJson);

            return Optional.of(responce);
        }
        return Optional.of(result);
    }


    private Optional<String> getFullMemeById(Context ctx){
        //meme+all attributes in other tables
        Meme meme = new Meme(JsonObject.fromJson(getMemeById(ctx).get()));
        if (ctx.getPathTokens().containsKey("userId")){

            String usrId = ctx.getPathTokens().get("userId");
            Optional<UserMemeRelation> relations = UserMemeRelationRecords.searchById(meme.getId(),usrId);
            if (relations.isPresent())
                meme.setLiked(true);
            return Optional.of(meme.jsonify().toString());
        }
        return Optional.of(meme.jsonify().toString());
    }

    private Optional<String> getMemeByTags(Context ctx){

        String[] tags=ctx.getPathTokens().get("memeParam").split("_");
        Optional<List<Meme>> memes = MemeRecords.searchByTags(Arrays.asList(tags));
        List<Meme> memeList = new ArrayList<Meme>();
        String response = "";
        if (memes.isPresent()){
            memeList = new ArrayList<Meme>(memes.get());

            for (Meme meme : memeList){
                response+=(meme.jsonify().toString());
            }

        }
        else
            return Optional.empty();

        return Optional.of(response);
    }

    @Override
    public void handle(Context ctx) throws Exception {
        Blocking.get(() -> {
            if (ctx.getRequest().getPath().contains("meme/system/name"))
                return getMemeByName(ctx);
            if (ctx.getRequest().getPath().contains("meme/system/id"))
                return getMemeById(ctx);
            if (ctx.getRequest().getPath().contains("meme/id"))
                return getFullMemeById(ctx);
            if (ctx.getRequest().getPath().contains("meme/name"))
                return getFullMemeByName(ctx);
            if (ctx.getRequest().getPath().contains("meme/system/tags"))
                return getMemeByTags(ctx);
            return Optional.empty();
        }).then(response ->{

            ctx.getResponse().contentType("application/json");
            if (response.isPresent()) {
                ctx.getResponse().status(200).send(response.get().toString());
                LOGGER.info(response.get().toString());
            }
            else
                ctx.getResponse().status(404).send("{\"MSG\": \"resource wasn't found\"}");
        });


    }
}
