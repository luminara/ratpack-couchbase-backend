package database.aws;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.s3.AmazonS3Client;

/**
 * Created by Oluwole on 26/06/2016.
 */
public class AmazonClientSingleton {
    private static AmazonDynamoDBClient dynamoDBClient = null;
    private static BasicAWSCredentials credentialsProvider = null;
    //private static AmazonSNSClient snsClient = null;
    private static AmazonS3Client s3Client = null;


    public static AmazonDynamoDBClient getDynamoDBClient() {
        if (dynamoDBClient==null){
            dynamoDBClient = new AmazonDynamoDBClient(getCredentialsProvider());
            dynamoDBClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
            //dynamoDBClient.setEndpoint(System.getenv("AWS_USER_LIST_ARN"));
        }
        return dynamoDBClient;
    }

    public static AmazonS3Client getS3Client() {
        if (s3Client==null){
            s3Client = new AmazonS3Client(getCredentialsProvider());
            s3Client.setRegion(Region.getRegion(Regions.EU_WEST_1));
            //dynamoDBClient.setEndpoint(System.getenv("AWS_USER_LIST_ARN"));
        }
        return s3Client;
    }
    //System.getenv("AWS_ACCESS_KEY_ID"), System.getenv("AWS_SECRET_ACCESS_KEY")
    //AKIAISVPTHPPAO6XQCEQ
    //h/PkB8T/k2hyXhyRW6CAMCJmtPjdZ1Eq5wwfMAa6
    public static BasicAWSCredentials getCredentialsProvider() {
        if (credentialsProvider==null){
            credentialsProvider = new BasicAWSCredentials("AKIAISVPTHPPAO6XQCEQ","h/PkB8T/k2hyXhyRW6CAMCJmtPjdZ1Eq5wwfMAa6");
        }
        return credentialsProvider;
    }

}