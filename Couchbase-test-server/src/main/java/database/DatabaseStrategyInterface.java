package database;

import Items.Storable;
import com.couchbase.client.java.document.json.JsonObject;

import java.util.List;
import java.util.Optional;

/**
 * Created by Oluwole on 29/12/2016.
 */
public interface DatabaseStrategyInterface{
    <T extends Storable> Optional<Boolean> upsert(T item);
    <T extends Storable> Optional<Boolean> delete(T item);
    //<T extends Storable> Optional<Storable<T>> get(T item, String param);
    Optional<List<JsonObject>> get(String query);
    Optional<JsonObject> getDocument(String id);
    //<T extends Storable> Optional<List<? extends Storable>> query(T item,QueryPath queryParam,String param);
}
