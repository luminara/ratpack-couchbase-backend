package database.mycouchbase;

import Items.Storable;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.couchbase.client.java.query.N1qlQueryRow;
import database.DatabaseStrategyInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by Oluwole on 28/12/2016.
 */
class CouchBaseDB implements DatabaseStrategyInterface {
    //private List<String> clusterIp;
    private String bucketName;
    private Bucket bucket;
    private CouchbaseCluster cluster;
    private static final Logger LOGGER = LoggerFactory.getLogger(CouchBaseDB.class);




    public CouchBaseDB(String bucketName,CouchbaseCluster cluster){
        //clusterIp=ip;
        this.bucketName = bucketName;
        bucket=cluster.openBucket(bucketName);
    }


    public CouchBaseDB(Bucket bucket){
        this.bucket=bucket;
    }


    public void disconnect(){
        bucket.close();
    }

    @Override
    public Optional<Boolean> upsert(Storable item) {
        JsonObject object = item.jsonify();
        // Store the Document
        if (bucket.exists(item.getId())) {
            JsonObject oldObj = bucket.get(item.getId()).content();
            oldObj.getNames().iterator().forEachRemaining(s -> {
                if (!object.containsKey(s)) {
                    object.put(s, oldObj.get(s));
                }
            });
        }
        bucket.upsert(JsonDocument.create(item.getId(),object));
        return Optional.of(Boolean.TRUE);
    }

    @Override
    public Optional<List<JsonObject>> get(String query) {
        N1qlQueryResult result = bucket.query(
                N1qlQuery.simple(query)
        );


        // Print each found Row
        List<JsonObject> jsonObjectList = new ArrayList<>();

        for (N1qlQueryRow row : result) {
            JsonObject object=row.value();
            object=object.getObject(bucketName);//goes down the json object hierarchy
            jsonObjectList.add(object);
            //break;
        }

        if (!jsonObjectList.isEmpty())
            return Optional.of(jsonObjectList);
        else
            return Optional.empty();
    }

    @Override
    public <T extends Storable> Optional<Boolean> delete(T item) {
        JsonDocument object = null;
        // Store the Document
        if (bucket.exists(item.getId())){
            object=bucket.remove(item.getId());
        }
        return (!object.content().isEmpty())?Optional.of(true):Optional.of(false);
    }

    @Override
    public Optional<JsonObject> getDocument(String id) {
        JsonDocument document = bucket.get(id);
        JsonObject object = document.content();


        if (!object.isEmpty())
            return Optional.of(object);
        else
            return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CouchBaseDB)) return false;
        CouchBaseDB that = (CouchBaseDB) o;
        return Objects.equals(bucketName, that.bucketName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bucketName);
    }

    //    @Override
//    public <T extends Storable> Optional<Storable<T>> get(T item, String param) {
//        N1qlQueryResult result = bucket.query(
//                N1qlQuery.simple("SELECT * FROM `"+bucketName+"` WHERE Name = \""+param+"\"")
//        );
//
//
//        // Print each found Row
//        JsonObject object=JsonObject.create();
//        for (N1qlQueryRow row : result) {
//            object=row.value();
//            object=object.getObject(bucketName);//goes down the json object hierarchy
//            break;
//        }
//
//        Storable storable = item.createFromJsonObject(object);
//        return Optional.of(storable);
//    }
//
//
//    @Override
//    public <T extends Storable> Optional<List<? extends Storable>> query(T item, QueryPath queryParam,String param) {
//        N1qlQueryResult result = bucket.query(
//                N1qlQuery.simple("SELECT * FROM `"+bucketName+"` WHERE "+queryParam.toString()+" = \""+param+"\"")
//        );
//
//
//        // Print each found Row
//        List<JsonObject> jsonObjectList = new ArrayList<>();
//        for (N1qlQueryRow row : result) {
//            // Prints {"name":"Arthur"}
//            JsonObject object=row.value();
//            object=object.getObject(bucketName);
//            jsonObjectList.add(object);
//            break;
//        }
//
//        List<Storable> storableArrayList = new ArrayList<>();
//        for (JsonObject jsonObject : jsonObjectList)
//            storableArrayList.add(item.createFromJsonObject(jsonObject));
//        return Optional.of(storableArrayList);
//    }
}
