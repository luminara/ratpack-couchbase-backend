package database.mycouchbase;

import Items.memes.Meme;
import com.couchbase.client.java.document.json.JsonObject;
import database.DatabaseStrategyInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Oluwole on 08/04/2017.
 */
public class MemeRecords {

    private static String bucketName = "ggg";
    private static DatabaseStrategyInterface database=CouchbaseSingleton.getDatabaseInstance(bucketName);;


    public static Optional<List<Meme>> searchByName(String name){
        List<Meme> memes = new ArrayList();
        Optional<List<JsonObject>> optional = database.get("SELECT * FROM `"+bucketName+"` WHERE Name LIKE ('%"+name+"%')");
        if (optional.isPresent()){
            for (JsonObject jsonObject : optional.get()){
                if (!memes.contains(new Meme(jsonObject)))
                    memes.add(new Meme(jsonObject));
            }
            return Optional.of(memes);
        }
        return Optional.empty();
    }


    public static Optional<List<Meme>> searchById(String id){
        List<Meme> memes = new ArrayList();
        Optional<List<JsonObject>> optional = database.get("SELECT * FROM `"+bucketName+"` WHERE Id = \""+id+"\"");
        if (optional.isPresent()){
            for (JsonObject jsonObject : optional.get()){
                memes.add(new Meme(jsonObject));
            }
            return Optional.of(memes);
        }
        return Optional.empty();
    }

    public static Optional<List<Meme>> searchByTags(List<String> tags){
       List<Meme> memes = new ArrayList();
        List<Optional<List<JsonObject>>> memeTags = new ArrayList<>();
        for (String tag : tags){
            memeTags.add(database.get("SELECT * FROM `"+bucketName+"`" +
                    " WHERE Tags LIKE ('%"+tag+"%')"));
        }
        for (Optional<List<JsonObject>> optional : memeTags){
            if (optional.isPresent()){
                for (JsonObject jsonObject : optional.get()){
                    if (!memes.contains(new Meme(jsonObject)))
                        memes.add(new Meme(jsonObject));
                }
            }
        }
        return (memes.size()>0)? Optional.of(memes):Optional.empty();
    }

    public static void save(Meme meme){
        database.upsert(meme);
    }

    public static void delete(Meme meme){
        database.delete(meme);
    }


}
