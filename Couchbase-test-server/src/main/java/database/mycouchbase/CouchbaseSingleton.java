package database.mycouchbase;

import com.couchbase.client.core.time.Delay;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import database.DatabaseStrategyInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Oluwole on 29/12/2016.
 */
class CouchbaseSingleton{
    private static CouchBaseDB database=null;
    private static String bucketName = "default";
    private static String url = "http://192.168.99.100:8091";//"http://couchbase-ELB-1997581356.eu-west-1.elb.amazonaws.com:8091";

    private static List<CouchBaseDB> dbList = new ArrayList<>();
    private static List<String> bucketList = new ArrayList<>();
    private static CouchbaseCluster cluster;


    private CouchbaseSingleton(){

    }

    public static DatabaseStrategyInterface getDatabaseInstance(String bucketName1){
        if (database==null || !bucketName.equals(bucketName1)){
            if (bucketList.contains(bucketName1)){
                database=dbList.get((bucketList.indexOf(bucketName1)));
            }else {
                database = new CouchBaseDB(bucketName1,cluster);
            }
            if (!dbList.contains(database)){
                dbList.add(database);
                bucketList.add(bucketName1);
            }
        }
        bucketName=bucketName1;

        return database;
    }


    public static void disconnect() {
        for (CouchBaseDB baseDB : dbList)
            baseDB.disconnect();
        //cluster.disconnect();
    }

    public static void couchbaseClusterSetup(){
        CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
                .socketConnectTimeout(30000)
                .kvTimeout(30000)
                .reconnectDelay(Delay.exponential(TimeUnit.MINUTES))
                .connectTimeout(30000) // default is 5s
                .build();

        List list = new ArrayList<String>();
        list.add(url);

        cluster = CouchbaseCluster.create(env,list);
    }


}
