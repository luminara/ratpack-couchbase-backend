package database.mycouchbase;

import Items.users.User;
import com.couchbase.client.java.document.json.JsonObject;
import database.DatabaseStrategyInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Oluwole on 08/04/2017.
 */
public class UserRecords {

    private static String bucketName = "default";
    private static DatabaseStrategyInterface database = CouchbaseSingleton.getDatabaseInstance(bucketName);;

    public static Optional<List<User>> searchById(String id){
        Optional<List<JsonObject>> optional = database.get("SELECT * FROM `"+bucketName+"` WHERE Id = \""+id+"\"");
        return optional.map(jsonObjectUsers -> jsonObjectUsers.stream()
        .map(jsonObjectUser -> new User(jsonObjectUser))
        .collect(Collectors.toList()));

    }

    public static Optional<List<User>> searchByName(String name){
        Optional<List<JsonObject>> optional = database.get("SELECT * FROM `"+bucketName+"` WHERE Name LIKE ('%"+name+"%')");
        return optional.map(jsonObjectUsers -> jsonObjectUsers.stream()
                .map(jsonObjectUser -> new User(jsonObjectUser))
                .collect(Collectors.toList()));
    }

    public static Optional<List<User>> searchByEmail(String email){
        List<User> users = new ArrayList();
        Optional<List<JsonObject>> optional = database.get("SELECT * FROM `"+bucketName+"` WHERE Email = \""+email+"\"");
        return optional.map(jsonObjectUsers -> jsonObjectUsers.stream()
                .map(jsonObjectUser -> new User(jsonObjectUser))
                .collect(Collectors.toList()));
    }

    public static Optional<List<User>> getAll(){
        List<User> users = new ArrayList();
        Optional<List<JsonObject>> optional = database.get("SELECT * FROM `"+bucketName+"` WHERE Email IS NOT null");
        return optional.map(jsonObjectUsers -> jsonObjectUsers.stream()
                .map(jsonObjectUser -> new User(jsonObjectUser))
                .collect(Collectors.toList()));
    }

    public static void save(User user){
        database.upsert(user);
    }

    public static void delete(User user){
       database.delete(user);
    }


}
