package database;

/**
 * Created by Oluwole on 14/01/2017.
 */
public enum QueryPath {
    QUERY_NAME("Name"),QUERY_ID("Id"),QUERY_EMAIl("Email");

    private String text;
    private QueryPath(String text){
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }
}
