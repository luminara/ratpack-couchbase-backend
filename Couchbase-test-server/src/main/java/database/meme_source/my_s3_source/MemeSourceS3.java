package database.meme_source.my_s3_source;

import database.FileStrategy;
import database.meme_source.MemeSource;

import java.util.Optional;

import static server.path.SourcePath.IMAGE_FILE_PATH;

/**
 * Created by Oluwole on 07/01/2017.
 */
public class MemeSourceS3 implements MemeSource {
    @Override
    public Optional<String> getSource() {
        return Optional.of(IMAGE_FILE_PATH.toString());
    }

    @Override
    public FileStrategy getFileStrategy() {
        return new FileStrategyS3();
    }

}
