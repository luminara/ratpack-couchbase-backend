package database.meme_source.my_s3_source;

import Items.BinaryItem;
import Items.Storable;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.*;
import com.couchbase.client.java.document.json.JsonObject;
import database.FileStrategy;
import database.aws.AmazonClientSingleton;
import database.meme_source.MemeSource;
import utils.ContentTypeExtentionConverter;
import utils.ExtensionNotFound;

import java.io.*;
import java.util.List;
import java.util.Optional;

/**
 * Created by Oluwole on 07/01/2017.
 */
public class FileStrategyS3 extends FileStrategy {

    @Override
    public Optional<String> saveFile(BinaryItem item) {
        try {
            BinaryItem.writeFile(item.getUploadedFile()).ifPresent(fileWrapper -> {
                //System.out.println(fileWrapper.getName());
                PutObjectRequest objectRequest = new PutObjectRequest(
                        item.getPath(), fileWrapper.getName(), fileWrapper.getFile());
                AmazonClientSingleton.getS3Client().putObject(objectRequest);
                //ctx.getResponse().status(200);
            });
        }catch (AmazonServiceException ase) {
            return Optional.empty();
        }catch (AmazonClientException ace) {
            return Optional.empty();
        }
        return Optional.of("OK");
    }

    @Override
    public Optional<File> getFile(MemeSource source,String id) {
        GetObjectRequest objectRequest = new GetObjectRequest(
                (String) source.getSource().get(), id);

        ResponseHeaderOverrides responseHeaders = new ResponseHeaderOverrides();


        S3Object s3object;
        try {
            s3object = AmazonClientSingleton.getS3Client().getObject(objectRequest);
        }catch (AmazonS3Exception e){
            return Optional.empty();
        }

        responseHeaders.setContentType(s3object.getObjectMetadata().getContentType());
        objectRequest.setResponseHeaders(responseHeaders);


        File file;
        try {
            file=writeTmpFile(s3object);
        } catch (IOException e) {
            return Optional.empty();
        }
        return Optional.of(file);

    }

    private static File writeTmpFile(S3Object s3object) throws IOException {

        File targetFile;
        try {
            targetFile = File.createTempFile("tmp",
                    ContentTypeExtentionConverter.getExtension(s3object.getObjectMetadata().getContentType()));
        } catch (ExtensionNotFound extensionNotFound) {
            throw new IOException();
        }
        InputStream inputStream = s3object.getObjectContent();
        byte[] buffer = new byte[inputStream.available()];


        int read=0;
        //File targetFile = new File("src/main/resources/targetFile.tmp");
        OutputStream outStream = new FileOutputStream(targetFile);
        while ((read = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, read);
        }
        outStream.flush();
        outStream.close();
        inputStream.close();
        return targetFile;
    }

    @Override
    public <T extends Storable> Optional<Boolean> delete(T item) {
        return null;
    }

    @Override
    public Optional<List<JsonObject>> get(String query) {
        return null;
    }

    @Override
    public Optional<JsonObject> getDocument(String id) {
        return null;
    }
}
