package database.meme_source;

import database.FileStrategy;

import java.util.Optional;

/**
 * Created by Oluwole on 07/01/2017.
 */
public interface MemeSource {
    public Optional<?> getSource();
    public FileStrategy getFileStrategy();
}
