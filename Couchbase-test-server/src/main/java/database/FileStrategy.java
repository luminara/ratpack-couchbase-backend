package database;

import Items.BinaryItem;
import Items.Storable;
import database.meme_source.MemeSource;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Created by Oluwole on 07/01/2017.
 */
public abstract class FileStrategy implements DatabaseStrategyInterface {

    @Override
    public Optional<Boolean> upsert(Storable item) {
        return null;
    }


    public <T extends Storable> Optional<Storable<T>> get(T item, String param) {
        return null;
    }


    public <T extends Storable> Optional<List<? extends Storable>> query(T item,QueryPath queryParam,String param) {
        return null;
    }

    public abstract Optional<String> saveFile(BinaryItem item);

    public abstract Optional<File> getFile(MemeSource source,String id);
}
