package utils;

import Items.memes.Meme;
import database.meme_source.MemeSource;

import java.io.File;

/**
 * Created by Oluwole on 07/01/2017.
 */
public class FileUtil {


    public static File getFile(MemeSource source, Meme item){
        return item.getBinaryItem().get(source,item.getId()).get();
    }
}
