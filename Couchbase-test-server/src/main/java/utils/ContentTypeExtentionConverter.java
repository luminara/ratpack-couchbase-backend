package utils;

/**
 * Created by Oluwole on 08/11/2016.
 */
public class ContentTypeExtentionConverter {
    private static String contentType;

    public static String getExtension(String contentType) throws ExtensionNotFound {
        switch (contentType) {
            case "image/jpeg":
                return ".jpg";
            case "image/png":
                return ".png";
            case "image/bmp":
                return  (".bmp");
            case "image/tiff":
                return (".tiff");
            case "audio/mp3":
                return ".mp3";
            case "audio/mpeg":
                return ".mp3";
            default:
                throw new ExtensionNotFound();
        }
    }

    public static String getContentType(String extension) throws ExtensionNotFound {
        switch (extension) {
            case ".jpg":
                return "image/jpeg";
            case ".png":
                return "image/png";
            case ".bmp":
                return  ("image/bmp");
            case ".tiff":
                return ("image/tiff");
            case ".mp3":
                return "audio/mp3";
            default:
                throw new ExtensionNotFound();
        }
    }
}
