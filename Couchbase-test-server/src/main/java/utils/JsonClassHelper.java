package utils;

import java.util.List;

/**
 * Created by Oluwole on 10/11/2016.
 */
public class JsonClassHelper {
    private static String getJsonList(List list){
        String tmp="";

        for (int i=0;i<list.size();i++){
            if (i<list.size()-1) {
                tmp += "\""+list.get(i)+ "\",";
            }
            else
                tmp += "\""+list.get(i)+"\"";
        }

        return tmp;
    }

    private static String jsonObjectList(List list){
        String tmp="";

        for (int i=0;i<list.size();i++){
            if (i<list.size()-1) {
                tmp += ""+list.get(i)+ ",";
            }
            else
                tmp += ""+list.get(i)+"";
        }

        return tmp;
    }

    public static String getJsonList(String key, List list){
        String tmp ="";
        if (key!=null) {
            if (list != null) {
                if (list.size() > 0) {
                    tmp = "\"" + key + "\":  [";
                    tmp += jsonObjectList(list);
                    tmp += "]";
                } else {
                    tmp = "\"" + key + "\" :" + null;
                }
            } else {
                tmp = "\"" + key + "\" :" + null;
            }
        }else {
            if (list != null) {
                if (list.size() > 0) {
                    tmp = "[";
                    tmp += jsonObjectList(list);
                    tmp += "]";
                } else {
                    tmp = "[" + "]";
                }
            } else {
                tmp = "[" + "]";
            }
        }

        return tmp;
    }
}
