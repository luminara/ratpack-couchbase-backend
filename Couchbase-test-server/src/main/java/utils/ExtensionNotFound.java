package utils;

/**
 * Created by Oluwole on 08/11/2016.
 */
public class ExtensionNotFound extends Exception {
    private final String TEXT="No file extension found for content-type";

    @Override
    public String getMessage() {
        return TEXT;
    }
}
