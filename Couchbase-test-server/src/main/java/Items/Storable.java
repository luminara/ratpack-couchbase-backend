package Items;

import com.couchbase.client.java.document.json.JsonObject;
import database.DatabaseStrategyInterface;

import java.util.Optional;

/**
 * Created by Oluwole on 29/12/2016.
 */
public interface Storable<T extends Storable> {
    Optional<Boolean> save(DatabaseStrategyInterface strategyInterface);
    Optional<Storable> get(DatabaseStrategyInterface strategyInterface, String id);
    T createFromJsonObject(JsonObject jsonObject);
    JsonObject jsonify();
    String getId();

}
