package Items;

import database.meme_source.my_s3_source.MemeSourceS3;
import ratpack.form.UploadedFile;

/**
 * Created by Oluwole on 08/11/2016.
 */
public class ImageBinaryItem extends BinaryItem{
    public ImageBinaryItem(UploadedFile uploadedFile) {
        super(uploadedFile);
    }
    @Override
    public String getPath() {
        MemeSourceS3 source = new MemeSourceS3();
        return source.getSource().get();
    }
}
