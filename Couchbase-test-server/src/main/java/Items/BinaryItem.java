package Items;

import database.FileStrategy;
import database.meme_source.MemeSource;
import ratpack.form.UploadedFile;
import server.path.SourcePath;
import utils.ContentTypeExtentionConverter;
import utils.ExtensionNotFound;

import java.io.*;
import java.util.Optional;

/**
 * Created by Oluwole on 08/11/2016.
 */
public abstract class BinaryItem {
    private UploadedFile uploadedFile;

    public BinaryItem(UploadedFile uploadedFile){
        this.uploadedFile=uploadedFile;
    }

    public Optional<String> save(FileStrategy strategyInterface){
        return strategyInterface.saveFile(this);
    }

    public Optional<File> get(MemeSource memeSource,String id){
        return memeSource.getFileStrategy().getFile(memeSource,id);
    }

    public UploadedFile getUploadedFile(){
        return uploadedFile;
    }

    public String getPath(){
        return SourcePath.ROOT_FILE_PATH.toString();
    }

    public static Optional<FileWrapper> writeFile(UploadedFile file) {
        File targetFile = null;
        String name = null;

        if (file==null)
            return Optional.empty();

        try {

            name=file.getFileName().substring(0,file.getFileName().lastIndexOf("."));//file.getFileName().split(".")[0];
            targetFile = File.createTempFile(name, ContentTypeExtentionConverter.getExtension(file.getContentType().getType()));


        } catch (IOException e) {
            return Optional.empty();
        } catch (ExtensionNotFound extensionNotFound) {
            return Optional.empty();
        }


        //File targetFile = new File("src/main/resources/targetFile.tmp");
        OutputStream outStream;
        try {
            outStream = new FileOutputStream(targetFile);
        } catch (FileNotFoundException e) {
            return Optional.empty();
        }

        try {
            file.writeTo(outStream);
        } catch (IOException e) {
            return Optional.empty();
        }
        try {
            outStream.flush();
            outStream.close();
        }catch (IOException e){
            return Optional.empty();
        }

        return Optional.of(new FileWrapper(name,targetFile));
    }

    public static class FileWrapper{
        private String name;
        private File file;
        public FileWrapper(String name, File file){
            this.name=name;
            this.file=file;
        }

        public File getFile() {
            return file;
        }

        public String getName() {
            return name;
        }
    }
}
