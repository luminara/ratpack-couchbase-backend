package Items.users;

import Items.JsonItemFields;
import Items.Storable;
import com.couchbase.client.java.document.json.JsonObject;
import com.fasterxml.jackson.databind.JsonNode;
import database.DatabaseStrategyInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created by Oluwole on 29/12/2016.
 */
public class User implements Storable<User> {
    protected String id;
    private String name;
    private String password;
    private String email;
    private List<String> urls=new ArrayList<>();

    public User(String id,String name){
        this.id = id;
        this.name = name;
    }

    public User(){

    }

    public User(JsonObject jsonObject){
        id=jsonObject.getString(JsonItemFields.ID.toString());
        name=jsonObject.getString(JsonItemFields.NAME.toString());
        password=jsonObject.getString(JsonItemFields.PASSWORD.toString());
        email=jsonObject.getString(JsonItemFields.EMAIL.toString());
        for (Object obj : jsonObject.getArray(JsonItemFields.USER_URLS.toString()).toList()){
            urls.add((String)obj);
        }
    }

    public User(JsonNode jsonNode){
        try {
            id = jsonNode.get(JsonItemFields.ID.toString()).asText();
        }catch (NullPointerException e){
            id=null;
        }
        try {
            name = jsonNode.get(JsonItemFields.NAME.toString()).asText();
        }catch(NullPointerException e){
            name=null;
        }
        try {
            password=jsonNode.get(JsonItemFields.PASSWORD.toString()).asText();
        }catch (NullPointerException e){}
        try {
            email=jsonNode.get(JsonItemFields.EMAIL.toString()).asText();
        }catch (NullPointerException e){}
        try {
            jsonNode.get(JsonItemFields.USER_URLS.toString()).elements().forEachRemaining(new Consumer<JsonNode>() {
                @Override
                public void accept(JsonNode jsonNode) {
                    urls.add(jsonNode.asText());
                }
            });
        }catch (NullPointerException e){}
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }


    @Override
    public Optional<Boolean> save(DatabaseStrategyInterface strategyInterface) {
        if (strategyInterface.upsert(this).isPresent())
            return Optional.of(Boolean.TRUE);
        else
            return Optional.of(Boolean.FALSE);
    }

    @Override
    public Optional<Storable> get(DatabaseStrategyInterface strategyInterface, String id) {
        return null;//Optional.of(strategyInterface.query(this,QueryPath.QUERY_ID,id).get().get(0));
    }

    @Override
    public User createFromJsonObject(JsonObject jsonObject) {
        return new User(jsonObject);
    }

    @Override
    public JsonObject jsonify() {
        JsonObject jsonObject = JsonObject.create();
        if (id!=null)
            jsonObject.put(JsonItemFields.ID.toString(),id);
        if (name!=null)
            jsonObject.put(JsonItemFields.NAME.toString(),name);
        if (email!=null)
            jsonObject.put(JsonItemFields.EMAIL.toString(),email);
        if (password!=null)
            jsonObject.put(JsonItemFields.PASSWORD.toString(),password);
        if (urls!=null)
            jsonObject.put(JsonItemFields.USER_URLS.toString(),urls);
        return jsonObject;
    }

    public void setId(String id) {
        this.id = id;
    }
}
