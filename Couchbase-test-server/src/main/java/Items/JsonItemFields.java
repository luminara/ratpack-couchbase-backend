package Items;

/**
 * Created by Oluwole on 07/11/2016.
 */
public enum JsonItemFields {
    ID("Id"),NAME("Name"),PASSWORD("Password"),EMAIL("Email"),MEME_IMAGE("MemeImage"),
    MEME_URL("Url"),MEMES_LIKED("ListOfLikedMemes"),MEME_ID("MemeId"),USER_ID("UserId"),
    LIKED("Like"),MEME_TAGS("Tags"),MATCH("Match"),USER1_ID("UserId1"),USER_URLS("Urls"),
    WEBSOCKET("WebSocket"),SESSION_USER_IDS("UserIds");

    private String text;
    JsonItemFields(String string){
        text=string;
    }

    @Override
    public String toString() {
        return text;
    }
}
