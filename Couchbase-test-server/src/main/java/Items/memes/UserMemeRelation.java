package Items.memes;

import Items.JsonItemFields;
import Items.Storable;
import com.couchbase.client.java.document.json.JsonObject;
import com.fasterxml.jackson.databind.JsonNode;
import database.DatabaseStrategyInterface;

import java.util.Optional;

/**
 * Created by Oluwole on 11/05/2017.
 */
public class UserMemeRelation  implements Storable<UserMemeRelation>{
    private String memeId;
    private String userId;

    public UserMemeRelation(String memeId,String usrId){
        this.memeId=memeId;
        userId = usrId;
    }

    public UserMemeRelation(JsonObject jsonObject){
        memeId=jsonObject.getString(JsonItemFields.MEME_ID.toString());
        userId=jsonObject.getString(JsonItemFields.USER_ID.toString());
    }

    public UserMemeRelation(JsonNode jsonNode){
        try {
            memeId = jsonNode.get(JsonItemFields.MEME_ID.toString()).asText();
        }catch (NullPointerException e){}
        try {
            userId = jsonNode.get(JsonItemFields.USER_ID.toString()).asText();
        }catch(NullPointerException e){}
    }



    @Override
    public Optional<Boolean> save(DatabaseStrategyInterface strategyInterface) {
        return null;
    }

    @Override
    public Optional<Storable> get(DatabaseStrategyInterface strategyInterface, String id) {
        return null;
    }

    @Override
    public UserMemeRelation createFromJsonObject(JsonObject jsonObject) {
        return new UserMemeRelation(jsonObject);
    }

    @Override
    public JsonObject jsonify() {
        return JsonObject.create()
                .put(JsonItemFields.MEME_ID.toString(),memeId)
                .put(JsonItemFields.USER_ID.toString(),userId);
    }

    @Override
    public String getId() {
        return memeId+userId;
    }
}
