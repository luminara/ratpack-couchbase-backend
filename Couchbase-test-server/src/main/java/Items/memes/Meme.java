package Items.memes;

import Items.BinaryItem;
import Items.ImageBinaryItem;
import Items.JsonItemFields;
import Items.Storable;
import com.couchbase.client.java.document.json.JsonObject;
import com.fasterxml.jackson.databind.JsonNode;
import database.DatabaseStrategyInterface;
import database.FileStrategy;
import database.meme_source.my_s3_source.FileStrategyS3;
import ratpack.form.Form;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created by Oluwole on 07/01/2017.
 */
public class Meme implements Storable<Meme> {
    private String id;
    private String name;
    private BinaryItem binaryItem;
    private String url;
    private boolean liked = false;
    private List<String> tags = new ArrayList<>();

    public Meme(String id,String name,BinaryItem item){
        this.id = id;
        this.name = name;
        this.binaryItem = item;
    }

    public Meme(String id,String name,String url){
        this.id = id;
        this.name = name;
        this.url = url;
    }

    public Meme(JsonObject jsonObject){
        id=jsonObject.getString(JsonItemFields.ID.toString());
        name=jsonObject.getString(JsonItemFields.NAME.toString());
        url=jsonObject.getString(JsonItemFields.MEME_URL.toString());
        for (Object obj : jsonObject.getArray(JsonItemFields.MEME_TAGS.toString()).toList()){
            tags.add((String)obj);
        }
    }

    public Meme(){

    }

    public Meme(Form form){
        id=form.get(JsonItemFields.ID.toString());
        name=form.get(JsonItemFields.NAME.toString());
        url=form.get(JsonItemFields.MEME_URL.toString());
        binaryItem= new ImageBinaryItem(form.file(JsonItemFields.MEME_IMAGE.toString()));
    }

    public Meme(JsonNode jsonNode){
        try {
            id = jsonNode.get(JsonItemFields.ID.toString()).asText();
        }catch (NullPointerException e){}
        try {
            name = jsonNode.get(JsonItemFields.NAME.toString()).asText();
        }catch(NullPointerException e){}
        try {
            url=jsonNode.get(JsonItemFields.MEME_URL.toString()).asText();
        }catch (NullPointerException e){}
        try {
            jsonNode.get(JsonItemFields.MEME_TAGS.toString()).elements().forEachRemaining(new Consumer<JsonNode>() {
                @Override
                public void accept(JsonNode jsonNode) {
                 tags.add(jsonNode.asText());
                }
            });
        }catch (NullPointerException e){}
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Optional<Boolean> save(DatabaseStrategyInterface strategyInterface) {
        if (strategyInterface.upsert(this).isPresent()) {
            //save file
            FileStrategy fileStrategy = new FileStrategyS3();
            binaryItem.save(fileStrategy);
            return Optional.of(Boolean.TRUE);
        }
        else
            return Optional.of(Boolean.FALSE);
    }

    @Override
    public Optional<Storable> get(DatabaseStrategyInterface strategyInterface, String id) {
        Meme item=null;// (Meme) strategyInterface.get(this,id).get();
        return Optional.of(item);
    }

    @Override
    public Meme createFromJsonObject(JsonObject jsonObject) {
        return new Meme(jsonObject);
    }

    @Override
    public JsonObject jsonify() {
        return JsonObject.create()
                .put(JsonItemFields.ID.toString(),id)
                .put(JsonItemFields.NAME.toString(),name)
                .put(JsonItemFields.MEME_URL.toString(),url)
                .put(JsonItemFields.LIKED.toString(),liked)
                .put(JsonItemFields.MEME_TAGS.toString(),tags);
    }

    @Override
    public String getId() {
        return id;
    }

    public BinaryItem getBinaryItem() {
        return binaryItem;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getUrl() {
        return url;
    }
}
