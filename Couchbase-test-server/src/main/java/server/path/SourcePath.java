package server.path;

/**
 * Created by Oluwole on 08/11/2016.
 */
public enum SourcePath implements Path{
    AUDIO_FILE_PATH("song-bucket"),
    IMAGE_FILE_PATH("root-image-bucket"),
    ROOT_FILE_PATH("root_folder");//never used

    private String text;
    private SourcePath(String string){
        text=string;
    }

    @Override
    public String toString() {
        return text;
    }
}
