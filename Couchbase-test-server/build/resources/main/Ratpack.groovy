import handlers.meme.method.get.MemeHandler
import handlers.meme.method.get.UserMemeRelationHandler
import handlers.meme.method.post.DeleteUserMemeRelationHandler
import handlers.meme.method.post.SaveMemeHandler
import handlers.meme.method.post.SaveUserMemeRelationHandler
import handlers.session.method.get.SessionHandler
import handlers.session.method.post.SaveSessionHandler
import handlers.user.method.get.UserHandler
import handlers.user.method.post.*
import ratpack.groovy.template.MarkupTemplateModule

import static ratpack.groovy.Groovy.ratpack

ratpack {
  bindings {
    module MarkupTemplateModule
    add(UserHandler.class,new UserHandler())
    add(SaveUserHandler.class,new SaveUserHandler())
    add(RegisterUserHandler.class,new RegisterUserHandler())
    add(LoginUserHandler.class,new LoginUserHandler())
    add(MemeHandler.class,new MemeHandler())
    add(SaveMemeHandler.class,new SaveMemeHandler())
    add(SaveUserMemeRelationHandler.class,new SaveUserMemeRelationHandler())
    add(UserMemeRelationHandler.class,new UserMemeRelationHandler())
    add(DeleteUserMemeRelationHandler.class,new DeleteUserMemeRelationHandler())
    add(SaveUserMatchRelationHandler.class,new SaveUserMatchRelationHandler())
    add(DeleteUserMatchRelationHandler.class,new DeleteUserMatchRelationHandler())
    //add(DatabaseBootService.class,new DatabaseBootService())
    add(SaveSessionHandler.class,new SaveSessionHandler())
    add(SessionHandler.class,new SessionHandler())

  }


  serverConfig {
    json("config.json")
    require("/",DatabaseConfig)
  }

  handlers {

    get("config") { DatabaseConfig config ->
      render toJson(config)
    }

    prefix("api/:apikey") {

      prefix("user") {
        post("register", RegisterUserHandler)
        post("save", SaveUserHandler);
        post("login", LoginUserHandler);
        get("email/:param", UserHandler)
        get("name/:param", UserHandler)
        get("id/:param", UserHandler)
        post("like",SaveUserMatchRelationHandler)
        post("dislike",DeleteUserMatchRelationHandler)
        prefix("system") {
          get("all", UserHandler)
        }
      }

      prefix("cache"){
        post("save/user/session",SaveSessionHandler)
        post("save/group/session",SaveSessionHandler)
        get("get/session/user/:userId",SessionHandler)
        get("get/session/group/:groupId",SessionHandler)

      }

      prefix("meme") {
        post("save", SaveMemeHandler)
        post("like", SaveUserMemeRelationHandler)
        post("dislike", DeleteUserMemeRelationHandler)
        get("name/:userId/:memeParam", MemeHandler)
        get("id/:userId/:memeParam", MemeHandler)
        get("/tags/:userId/:memeParam",MemeHandler)

        prefix("system") {
          get("name/:memeParam", MemeHandler)
          get("id/:memeParam", MemeHandler)
          get("tags/:memeParam",MemeHandler)

          prefix("liked") {
            get("memeId/:memeId", UserMemeRelationHandler)
            get("userId/:userId", UserMemeRelationHandler)
            get("fullId/:memeId/:userId", UserMemeRelationHandler)
          }

        }
      }

    }

    files { dir "public" }
  }
}

class DatabaseConfig {
  String port = 8051
  String user = "root"
  String password
  String db = "myDB"
}