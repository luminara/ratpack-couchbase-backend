DankLove is a dating app where matches are found by simply looking at memes all day long.

This is done via the use of the "meme coefficient" that is then ran against a list of other users in search of similar coefficient value to yours.

What's the meme coefficient?

The meme coefficient is the value that identifies the type of memes the user appreciates, allowing us to search for similar users for possible matches.


Matches:

As soon as you get a match you'll be able to start chatting with one another.


Platforms:

I plan the app to be both on mobile devices (Android) and on a website, in fact I've already started writing the code for the website's front and back end.


Backend: 

Test Server built on Ratpack 1.4.+ and java 1.8.+, provides an interface to fetch and insert data in a local couchbase 4.5 server datbase.
The database can be reached via HTTP (I'm still in development phase, if I reach production it's going to change to HTTPS).
The server.jar is compressed with a Dockerfile allowing to start the server on AWS Elastic BeanStalk instance running Docker.